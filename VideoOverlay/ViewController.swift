//
//  ViewController.swift
//  VideoOverlay
//
//  Created by Oleg Ulitsionak on 6/12/15.
//  Copyright (c) 2015 Oleg Ulitsionak. All rights reserved.
//

import UIKit
import MobileCoreServices
import Player
import FBSDKShareKit
import FBSDKCoreKit
import FBSDKLoginKit
import SnapKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        if VideoService.shared.videoURL == nil {
            pickVideo()
        }
    }
    
    func pickVideo() {

        var picker = UIImagePickerController()
        let sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        if (UIImagePickerController.isSourceTypeAvailable(sourceType))
        {
            picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            picker.videoQuality = .TypeHigh
            picker.mediaTypes = [kUTTypeMovie]
            picker.delegate = self
            
            self.presentViewController(picker, animated: true,
                completion: nil)
        }
        
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        
        var player = Player()
        player.view.frame = self.view.bounds
        self.addChildViewController(player)
        self.view.addSubview(player.view)
        player.didMoveToParentViewController(self)
        
        VideoService.shared.videoURL = info["UIImagePickerControllerMediaURL"] as! NSURL
        VideoService.shared.renderVideo({ (outputURL) -> Void in
            player.path = outputURL.path
            picker.dismissViewControllerAnimated(true, completion: { () -> Void in
                player.playFromBeginning()
            })
        }, assetCompletion: { (outputURL) -> Void in
            
            var video : FBSDKShareVideo = FBSDKShareVideo()
            video.videoURL = outputURL
            var content : FBSDKShareVideoContent = FBSDKShareVideoContent()
            content.video = video
            
            var button : FBSDKShareButton = FBSDKShareButton()
            button.shareContent = content
            self.view.addSubview(button)
            button.snp_makeConstraints { make in
                make.centerX.equalTo(self.view)
                make.bottom.equalTo(self.view).offset(-100)
                make.width.equalTo(200)
                make.height.equalTo(50)
            }
        })
        
    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }

    
}

