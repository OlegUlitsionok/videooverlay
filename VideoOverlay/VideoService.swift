//
//  VideoService.swift
//  VideoOverlay
//
//  Created by Oleg Ulitsionak on 6/12/15.
//  Copyright (c) 2015 Oleg Ulitsionak. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import AssetsLibrary

let kTempFileName = "VideoOverlay.mov"

private let _VideoServiceSharedInstance = VideoService()

public class VideoService {
    
    class var shared : VideoService {
        return _VideoServiceSharedInstance
    }
    
    var videoAsset : AVAsset!
    
    public var videoURL : NSURL! {
        willSet(newURL) {
            self.videoAsset = AVAsset.assetWithURL(newURL) as! AVAsset
        }
    }
    
    public var renderedVideoURL : NSURL!

    init() {
    
    }
    
    public func renderVideo(completion: (outputURL: NSURL) -> Void, assetCompletion: (outputURL: NSURL) -> Void) {
        
        var type = AVMediaTypeVideo
        var range : CMTimeRange = CMTimeRangeMake(kCMTimeZero, self.videoAsset.duration)
        var track : AVAssetTrack = self.videoAsset.tracksWithMediaType(AVMediaTypeVideo)![0] as! AVAssetTrack

        var mainInstruction : AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, self.videoAsset.duration)
        mainInstruction.backgroundColor = UIColor.blackColor().CGColor
        
        var videolayerInstruction : AVMutableVideoCompositionLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
        videolayerInstruction.setOpacity(1.0, atTime: self.videoAsset.duration)
        mainInstruction.layerInstructions = [videolayerInstruction]
        
        var mixComposition : AVMutableComposition = AVMutableComposition()
        var videoTrack : AVMutableCompositionTrack = mixComposition.addMutableTrackWithMediaType(type, preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        videoTrack.insertTimeRange(range, ofTrack: track, atTime: kCMTimeZero, error: nil)
        
        var mainCompositionInst : AVMutableVideoComposition = AVMutableVideoComposition()
        mainCompositionInst.instructions = [mainInstruction]
        mainCompositionInst.renderSize = track.naturalSize
        mainCompositionInst.frameDuration = CMTimeMake(1, 30)
        
        var overlayLayer : CALayer = CALayer()
        overlayLayer.frame = CGRect(origin: CGPointZero, size: track.naturalSize)
        overlayLayer.contents = UIImage(named:"overlayImage")!.CGImage
        overlayLayer.masksToBounds = true
        overlayLayer.opacity = 0.5

        var parentLayer = CALayer()
        var videoLayer = CALayer()
        parentLayer.frame = CGRect(origin: CGPointZero, size: track.naturalSize)
        videoLayer.frame = parentLayer.frame
        parentLayer.addSublayer(videoLayer)
        parentLayer.addSublayer(overlayLayer)
        
        mainCompositionInst.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, inLayer: parentLayer)
        
        var tempDir = NSTemporaryDirectory()
        var path = tempDir.stringByAppendingPathComponent(kTempFileName)
        var tempURL = NSURL(fileURLWithPath: path)
        
        if NSFileManager().fileExistsAtPath(tempURL!.path!) {
            NSFileManager().removeItemAtPath(tempURL!.path!, error: nil)
        }
        
        var exporter : AVAssetExportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)
        exporter.outputURL = tempURL
        exporter.outputFileType = AVFileTypeQuickTimeMovie
        exporter.videoComposition = mainCompositionInst
        exporter.exportAsynchronouslyWithCompletionHandler { () -> Void in
            self.renderedVideoURL = exporter.outputURL
            completion(outputURL: exporter.outputURL)
            var library : ALAssetsLibrary = ALAssetsLibrary()
            if library.videoAtPathIsCompatibleWithSavedPhotosAlbum(self.renderedVideoURL) {
                library.writeVideoAtPathToSavedPhotosAlbum(self.renderedVideoURL, completionBlock: { (url :NSURL!, error : NSError!) -> Void in
                    assetCompletion(outputURL: url)
                })
            }
        }
    }
    
}